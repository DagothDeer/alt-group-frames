; This Add-on is not created by, affiliated with or sponsored by ZeniMax Media
; Inc. or its affiliates. The Elder Scrolls and related logos are registered
; trademarks or trademarks of ZeniMax Media Inc. in the United States and/or
; other countries. All rights reserved.
; https://account.elderscrollsonline.com/add-on-terms

## APIVersion: 101033
## Title: Alternative Group Frames : Buffs
## Author: |c943810BulDeZir|r
## Version: 1.0
## SavedVariables: AltGroupFramesBuffTrackerSV
## DependsOn: LibAddonMenu-2.0 AltGroupFrames

BuffTracker.lua
BuffTracker.xml
